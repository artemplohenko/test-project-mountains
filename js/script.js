$(document).ready(function(){

	$('.toggle-menu').click(function () {
		$('.menu-container').toggleClass('menu-open');
		$(this).toggleClass('active');
	});

	$(".menu").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 1500);
	});

});